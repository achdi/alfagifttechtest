//
//  CategoryViewController.swift
//  alfagift
//
//  Created by Achdi Febriansyah on 10/09/22.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa

class CategoryViewController: UIViewController {
    private var viewModel: CategoryViewModel
    private let disposeBag = DisposeBag()

    let activityIndicator: UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.medium)
        return activityIndicator
    }()

    override var prefersStatusBarHidden: Bool {
        return true
    }

    override func viewDidLoad() {
        super.viewDidLoad()

//        activityIndicator.center = view.center
        setupBinding()
    }
    
    private func setupBinding(){
        view = CategoryView()
        self.navigationItem.titleView = (self.view as! CategoryView).searchBar
        let tableView = (self.view as! CategoryView).categoryTableView
        let searchField = (self.view as! CategoryView).searchBar.searchTextField
        
        viewModel.output
            .targets.asDriver(onErrorJustReturn: [])
            .drive(tableView.rx.items(cellIdentifier: "CategoryTableCell", cellType: CategoryTableCell.self)) {  (row,category,cell) in
                    (cell as CategoryTableCell).setCategoryLabelText(category)
                
                    (cell as CategoryTableCell).tapGesture.rx.event.bind(onNext: { recognizer in
                        print("touches: \(recognizer.numberOfTouches)") //or whatever you like
                        let vc = SourceViewController(category: category)
                        self.navigationController?.pushViewController(vc, animated: true)
                    }).disposed(by: self.disposeBag)
                }
            .disposed(by: disposeBag)
        

        searchField.rx.text
            .orEmpty
            .throttle(RxTimeInterval.microseconds(1), scheduler: MainScheduler.instance)
            .distinctUntilChanged()
            .bind(to: viewModel.input.search)
            .disposed(by: disposeBag)
    }
    
    
    init() {
        viewModel = CategoryViewModel()
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        NotificationCenter.default.addObserver(self, selector: #selector(CategoryViewController.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(CategoryViewController.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func keyboardWillShow(notification: Notification) {
        if let keyboardHeight = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height {
            print("Notification: Keyboard will show")
            (self.view as! CategoryView).categoryTableView.setBottomInset(to: keyboardHeight)
        }
    }

    @objc func keyboardWillHide(notification: Notification) {
        print("Notification: Keyboard will hide")
        (self.view as! CategoryView).categoryTableView.setBottomInset(to: 0.0)
    }
}
