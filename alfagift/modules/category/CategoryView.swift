//
//  CategoryView.swift
//  alfagift
//
//  Created by Achdi Febriansyah on 10/09/22.
//

import UIKit

class CategoryView: UIView {

    var topView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = #colorLiteral(red: 0.05882352963, green: 0.180392161, blue: 0.2470588237, alpha: 1)
        return view
    }()
    
    var navBar: UINavigationBar = {
        let view = UINavigationBar()
        view.backgroundColor = #colorLiteral(red: 0.05882352963, green: 0.180392161, blue: 0.2470588237, alpha: 1)
        return view
    }()

    var searchBar: UISearchBar = {
        let searchBar = UISearchBar()
        searchBar.translatesAutoresizingMaskIntoConstraints = false
        searchBar.searchBarStyle = .minimal
//        searchBar.backgroundColor = .gray
        let textField = searchBar.value(forKey: "searchField") as! UITextField
        textField.textColor = .white
        
        let glassIconView = textField.leftView as! UIImageView
        glassIconView.image = glassIconView.image?.withRenderingMode(.alwaysTemplate)
        glassIconView.tintColor = UIColor.white
        return searchBar
    }()

    var categoryTableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.register(CategoryTableCell.self, forCellReuseIdentifier: CategoryTableCell.identifier)

        tableView.estimatedRowHeight = 200
        tableView.rowHeight = UITableView.automaticDimension
        tableView.allowsSelection = false
        return tableView
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        setTopView()
        setTableView()
    }

    private func setTopView() {
        addSubview(topView)
        topView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        topView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        topView.widthAnchor.constraint(equalTo: widthAnchor).isActive = true
        topView.heightAnchor.constraint(equalToConstant: 60).isActive = true
    }

    private func setTableView() {
        addSubview(categoryTableView)
        categoryTableView.topAnchor.constraint(equalTo: topView.bottomAnchor).isActive = true
        categoryTableView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        categoryTableView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        categoryTableView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
    }

    private func setSearchBar() {
        topView.addSubview(searchBar)
        searchBar.trailingAnchor.constraint(equalTo: topView.trailingAnchor, constant: -5).isActive = true
        searchBar.topAnchor.constraint(equalTo: topView.topAnchor, constant: 10).isActive = true
        searchBar.leadingAnchor.constraint(equalTo: topView.leadingAnchor, constant: 5).isActive = true
        searchBar.heightAnchor.constraint(equalToConstant: 40).isActive = true
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    func setTableViewDataSourceDelegate <obj: UITableViewDataSource & UITableViewDelegate> (
        dataSourceDelegate: obj) {
            categoryTableView.dataSource = dataSourceDelegate
            categoryTableView.delegate = dataSourceDelegate
            categoryTableView.reloadData()
    }

}
