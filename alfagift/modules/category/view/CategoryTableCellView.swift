//
//  CategoryTableCellView.swift
//  alfagift
//
//  Created by Achdi Febriansyah on 10/09/22.
//

import UIKit
import CoreData
import RxCocoa
import Foundation

class CategoryTableCell: UITableViewCell {

    public static let identifier = "CategoryTableCell"
    
    let tapGesture = UITapGestureRecognizer()

    private var categoryLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .boldSystemFont(ofSize: 30)
        return label
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addGestureRecognizer(tapGesture)
        setLabel()
    }

    private func setLabel() {
        addSubview(categoryLabel)
        categoryLabel.topAnchor.constraint(equalTo: topAnchor, constant: 5).isActive = true
        categoryLabel.heightAnchor.constraint(equalToConstant: 30).isActive = true
        categoryLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10).isActive = true
        categoryLabel.widthAnchor.constraint(equalTo: widthAnchor).isActive = true
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    public func setCategoryLabelText(_ text: String?) {
        categoryLabel.text = text
    }
}
