//
//  CategoryViewModel.swift
//  alfagift
//
//  Created by Achdi Febriansyah on 10/09/22.
//

import Foundation
import RxSwift
import RxCocoa

class CategoryViewModel {
    let input: Input
    let output: Output
    let categories = ["business", "entertainment", "general", "health", "science", "sports", "technology"]

    struct Input {
        let search: PublishRelay<String>
    }

    struct Output {
        let targets: Driver<[String]>
    }
    
    init() {

        let targetSubject = PublishRelay<[String]>()
        let searchSubject = PublishRelay<String>()

        let targets = searchSubject
            .startWith("")
            .distinctUntilChanged()
            .withLatestFrom(targetSubject.startWith(categories).asObservable()) { (search, categories) in (search, categories)
            }
            .map({ (search, categories) -> [String] in
                return categories.filter({ $0.lowercased().hasPrefix(search.lowercased()) || $0.lowercased().contains("\(search.lowercased())") })
            })
            .asDriver(onErrorJustReturn: [])

        self.input = Input(search: searchSubject)
        self.output = Output(targets: targets)
    }
    
}
