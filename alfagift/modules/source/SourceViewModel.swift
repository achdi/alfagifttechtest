//
//  SourceViewModel.swift
//  alfagift
//
//  Created by Achdi Febriansyah on 10/09/22.
//

import Foundation
import RxSwift
import RxCocoa

class SourceViewModel {
   
    private var category: String
    private let managerConnections = ManagerConnections()
    
    init(category: String) {
        self.category = category
    }
    
    func getSourceList() -> Observable<[Source]> {
        return managerConnections.getSources(category: category)
    }
}
