//
//  SourceTableCellView.swift
//  alfagift
//
//  Created by Achdi Febriansyah on 10/09/22.
//

import UIKit

class SourceTableCell: UITableViewCell {

    public static let identifier = "SourceTableCell"
    let tapGesture = UITapGestureRecognizer()
    private var sourceLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .boldSystemFont(ofSize: 30)
        return label
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addGestureRecognizer(tapGesture)
        setLabel()
    }

    private func setLabel() {
        addSubview(sourceLabel)
        sourceLabel.topAnchor.constraint(equalTo: topAnchor, constant: 5).isActive = true
        sourceLabel.heightAnchor.constraint(equalToConstant: 30).isActive = true
        sourceLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10).isActive = true
        sourceLabel.widthAnchor.constraint(equalTo: widthAnchor).isActive = true
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    public func setSourceLabelText(_ text: String?) {
        sourceLabel.text = text
    }
}
