//
//  SourceViewController+Other.swift
//  alfagift
//
//  Created by Achdi Febriansyah on 11/09/22.
//

import Foundation
import UIKit
import RxSwift

extension SourceViewController: UISearchBarDelegate {
    public func displaySources() {
        return viewModel.getSourceList()
          // Handle RxSwift concurrency, execute on Main Thread
          .subscribe(on: MainScheduler.instance)
          .observe(on: MainScheduler.instance)
          // Subscribe observer to Observable
          .subscribe(
            onNext: { [weak self] sources in
              self?.sources = sources
              print("sources in view: \(sources.map { $0.name })")
              self?.updateTableView()
            }, onError: { error in
              print("error in view: \(error)")
                DispatchQueue.main.async { [weak self] in
                    self?.activityIndicator.stopAnimating()
                    self?.activityIndicator.isHidden = true
                    (self?.view as! SourceView).imageError.isHidden = false
                }
              // Finalize the RxSwift sequence (Disposable)
            }, onCompleted: {}).disposed(by: disposeBag)
      }
    
    private func updateTableView() {
        DispatchQueue.main.async { [weak self] in
            (self?.view as! SourceView).sourceTableView.reloadData()
            self?.activityIndicator.stopAnimating()
            self?.activityIndicator.isHidden = true
        }
      }
    
    public func configureVC() {
        configureNavBar()
        configureSearchController()
    }

    private func configureNavBar() {
        navigationItem.title = "Source"
    }

    private func configureSearchController() {
        let searchBar = searchController.searchBar
        let appearance = UINavigationBarAppearance()
        appearance.configureWithOpaqueBackground()
        appearance.titleTextAttributes = [.foregroundColor: UIColor.white]
        navigationController?.navigationBar.standardAppearance = appearance
        searchBar.tintColor = .white
        
        searchBar.delegate = self
        
        navigationItem.searchController = searchController
        searchBar.translatesAutoresizingMaskIntoConstraints = false
        searchBar.searchBarStyle = .minimal
        searchBar.backgroundColor =  #colorLiteral(red: 0.05882352963, green: 0.180392161, blue: 0.2470588237, alpha: 1)
        let textField = searchBar.value(forKey: "searchField") as! UITextField
        textField.textColor = .white

        let glassIconView = textField.leftView as! UIImageView
        glassIconView.image = glassIconView.image?.withRenderingMode(.alwaysTemplate)
        glassIconView.tintColor = UIColor.white
        searchBar.heightAnchor.constraint(equalToConstant: 40).isActive = true
    //    tableView.tableHeaderView = searchBar
    //    tableView.contentOffset = CGPoint(x: 0, y: searchBar.frame.size.height)
        searchBar.rx.text
          .orEmpty
          .distinctUntilChanged()
          // When the text of the search bar gets updated
          .subscribe(onNext: { [weak self] result in
            guard let strongSelf = self else { return }
            print("Updated searchbar to: \(result)")
            strongSelf.filteredSources = strongSelf.sources.filter { source in
              strongSelf.updateTableView()
              return source.name.contains(result)
            }
          }).disposed(by: disposeBag)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
//    searchController.isActive = false
        print("Filtered source: \(filteredSources.map { $0.name })")
        guard sources.count == filteredSources.count else { return  }
        updateTableView()
    }
}
