//
//  SourceViewController+UITableView.swift
//  alfagift
//
//  Created by Achdi Febriansyah on 11/09/22.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa

extension SourceViewController: UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if searchController.isActive, searchController.searchBar.text != "" {
      return filteredSources.count
    }
    return sources.count
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let cellIdentifier: String = SourceTableCell.identifier
      guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? SourceTableCell else {
          fatalError("Error dequing cell: \(cellIdentifier)")
      }
      
      var source = sources[indexPath.row]
      if searchController.isActive, searchController.searchBar.text != "" {
          source = filteredSources[indexPath.row]
      }
      
      (cell as SourceTableCell).tapGesture.rx.event.bind(onNext: { recognizer in
          print("touches: \(recognizer.numberOfTouches)") //or whatever you like
          let vc = ArticleViewController(sourceId: source.id)
          self.navigationController?.pushViewController(vc, animated: true)
      }).disposed(by: self.disposeBag)
      
      
      (cell as SourceTableCell).setSourceLabelText(source.name)
      
      return cell
  }
}

extension SourceViewController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 50
  }

  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    var source = sources[indexPath.row]
    if searchController.isActive, searchController.searchBar.text != "" {
        source = filteredSources[indexPath.row]
    }
//    tableView.tableHeaderView = nil
//    router.naviagteToDetailView(movieId: String(movie.movieId))
  }
}
