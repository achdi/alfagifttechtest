//
//  ArticleTableCellView.swift
//  alfagift
//
//  Created by Achdi Febriansyah on 11/09/22.
//

import UIKit

class ArticleTableCell: UITableViewCell {

    public static let identifier = "ArticleTableCell"
    
    let tapGesture = UITapGestureRecognizer()

    private var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .boldSystemFont(ofSize: 30)
        label.lineBreakMode = .byTruncatingTail
        return label
    }()
    
    private var descText: UILabel = {
        let desc = UILabel()
        desc.translatesAutoresizingMaskIntoConstraints = false
        desc.textColor = .gray
        return desc
    }()
    
    private var dateText: UILabel = {
        let date = UILabel()
        date.translatesAutoresizingMaskIntoConstraints = false
        date.textColor = .gray
        return date
    }()
    
    
    let titleImageView = ScaleAspectFitImageView.init(image: UIImage(named: "blank"))

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addGestureRecognizer(tapGesture)
        setupTitleImageLabel()
        setLabel()
        setDescText()
        setDateText()
    }
    
    private func setupTitleImageLabel() {
        let marginGuide = layoutMarginsGuide
        addSubview(titleImageView)
        titleImageView.translatesAutoresizingMaskIntoConstraints = false
        titleImageView.clipsToBounds = true
        titleImageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 19).isActive = true
//        titleImageView.trailingAnchor.constraint(equalTo: marginGuide.leadingAnchor, constant: 440).isActive = true
        titleImageView.bottomAnchor.constraint(equalTo: bottomAnchor,constant: 10).isActive = true
        titleImageView.topAnchor.constraint(equalTo: topAnchor, constant: 5).isActive = true
        titleImageView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        titleImageView.widthAnchor.constraint(equalToConstant: 120).isActive = true
    }

    private func setLabel() {
        let marginGuide = layoutMarginsGuide
        addSubview(titleLabel)
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.leadingAnchor.constraint(equalTo: titleImageView.trailingAnchor, constant: 15).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: marginGuide.trailingAnchor).isActive = true
        titleLabel.topAnchor.constraint(equalTo: marginGuide.topAnchor, constant: 10).isActive = true
        titleLabel.heightAnchor.constraint(equalToConstant: 18).isActive = true
        titleLabel.font = .boldSystemFont(ofSize: 16)
    }
    
    private func setDescText() {
        let marginGuide = layoutMarginsGuide
        addSubview(descText)
        descText.translatesAutoresizingMaskIntoConstraints = false
        descText.leadingAnchor.constraint(equalTo: titleImageView.trailingAnchor, constant: 15).isActive = true
        descText.trailingAnchor.constraint(equalTo: marginGuide.trailingAnchor).isActive = true
        descText.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 5).isActive = true
        descText.numberOfLines = 5
        descText.font = .systemFont(ofSize: 12)
        descText.textColor = UIColor.lightGray
    }
    
    private func setDateText() {
        let marginGuide = layoutMarginsGuide
        addSubview(dateText)
        dateText.translatesAutoresizingMaskIntoConstraints = false
//        descText.leadingAnchor.constraint(equalTo: titleImageView.trailingAnchor, constant: 15).isActive = true
        dateText.trailingAnchor.constraint(equalTo: marginGuide.trailingAnchor).isActive = true
        dateText.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -15).isActive = true
        dateText.numberOfLines = 0
        dateText.font = .systemFont(ofSize: 12)
        dateText.textColor = UIColor.lightGray
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    public func setText(_ title: String?,_ desc: String?,_ imageURL: String,_ pubDate: String) {
        titleLabel.text = title
        descText.text = desc
        titleImageView.downloaded(from: imageURL, contentMode: .scaleAspectFit)
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"

        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd MMMM yyyy HH:mm"

        if let date = dateFormatterGet.date(from: pubDate) {
            dateText.text = dateFormatterPrint.string(from: date)
        } else {
            dateText.text = "Uknown date"
        }
    }
}
