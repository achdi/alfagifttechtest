//
//  ArticleViewController+UITableView.swift
//  alfagift
//
//  Created by Achdi Febriansyah on 11/09/22.
//

import Foundation
import UIKit

extension ArticleViewController: UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if searchController.isActive, searchController.searchBar.text != "" {
      return filteredArticles.count
    }
    return articles.count
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cellIdentifier: String = ArticleTableCell.identifier
    guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? ArticleTableCell else {
      fatalError("Error dequing cell: \(cellIdentifier)")
    }
      
    var article = articles[indexPath.row]
    if searchController.isActive, searchController.searchBar.text != "" {
        article = filteredArticles[indexPath.row]
    }
      (cell as ArticleTableCell).tapGesture.rx.event.bind(onNext: { recognizer in
          print("touches: \(recognizer.numberOfTouches)") //or whatever you like
          let vc = WebviewViewController(urlString: article.url!)
          self.navigationController?.pushViewController(vc, animated: true)
      }).disposed(by: self.disposeBag)
      
      (cell as ArticleTableCell).setText(article.title, article.description, article.urlToImage!, article.publishedAt!)
      
    return cell
  }
}

extension ArticleViewController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 150
  }

  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    var article = articles[indexPath.row]
    if searchController.isActive, searchController.searchBar.text != "" {
        article = filteredArticles[indexPath.row]
    }
//    tableView.tableHeaderView = nil
//    router.naviagteToDetailView(movieId: String(movie.movieId))
  }
}
