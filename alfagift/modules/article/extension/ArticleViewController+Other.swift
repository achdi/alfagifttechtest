//
//  ArticleViewController+Other.swift
//  alfagift
//
//  Created by Achdi Febriansyah on 11/09/22.
//

import Foundation
import UIKit
import RxSwift

extension ArticleViewController: UISearchBarDelegate {
    public func displayArticles() {
        return viewModel.getArticleList()
          // Handle RxSwift concurrency, execute on Main Thread
          .subscribe(on: MainScheduler.instance)
          .observe(on: MainScheduler.instance)
          // Subscribe observer to Observable
          .subscribe(
            onNext: { [weak self] articles in
              self?.articles = articles
              print("articles in view: \(articles.map { $0.title })")
              self?.updateTableView()
            }, onError: { error in
                print("error in view: \(error)")
                DispatchQueue.main.async { [weak self] in
                    self?.activityIndicator.stopAnimating()
                    self?.activityIndicator.isHidden = true
                    (self?.view as! ArticleView).imageError.isHidden = false
                }
            }, onCompleted: {}).disposed(by: disposeBag)
      }
    
    private func updateTableView() {
        DispatchQueue.main.async { [weak self] in
            (self?.view as! ArticleView).tableView.reloadData()
            self?.activityIndicator.stopAnimating()
            self?.activityIndicator.isHidden = true
        }
      }
    
    public func configureVC() {
        configureNavBar()
        configureSearchController()
    }

    private func configureNavBar() {
        navigationItem.title = "Article"
    }

    private func configureSearchController() {
        let searchBar = searchController.searchBar
        let appearance = UINavigationBarAppearance()
        appearance.configureWithOpaqueBackground()
        appearance.titleTextAttributes = [.foregroundColor: UIColor.white]
        navigationController?.navigationBar.standardAppearance = appearance
        searchBar.tintColor = .white
        
        searchBar.delegate = self
        
        navigationItem.searchController = searchController
        searchBar.translatesAutoresizingMaskIntoConstraints = false
        searchBar.searchBarStyle = .minimal
        searchBar.backgroundColor =  #colorLiteral(red: 0.05882352963, green: 0.180392161, blue: 0.2470588237, alpha: 1)
        let textField = searchBar.value(forKey: "searchField") as! UITextField
        textField.textColor = .white

        let glassIconView = textField.leftView as! UIImageView
        glassIconView.image = glassIconView.image?.withRenderingMode(.alwaysTemplate)
        glassIconView.tintColor = UIColor.white
        searchBar.heightAnchor.constraint(equalToConstant: 40).isActive = true
    //    tableView.tableHeaderView = searchBar
    //    tableView.contentOffset = CGPoint(x: 0, y: searchBar.frame.size.height)
        searchBar.rx.text
          .orEmpty
          .distinctUntilChanged()
          // When the text of the search bar gets updated
          .subscribe(onNext: { [weak self] result in
            guard let strongSelf = self else { return }
            print("Updated searchbar to: \(result)")
            strongSelf.filteredArticles = strongSelf.articles.filter { article in
              strongSelf.updateTableView()
                return article.description!.contains(result) || article.title!.contains(result)
            }
          }).disposed(by: disposeBag)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
//    searchController.isActive = false
        print("Filtered article: \(filteredArticles.map { $0.title })")
        guard articles.count == filteredArticles.count else { return  }
        updateTableView()
    }
}
