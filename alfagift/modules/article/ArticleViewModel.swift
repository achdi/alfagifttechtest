//
//  ArticleViewModel.swift
//  alfagift
//
//  Created by Achdi Febriansyah on 11/09/22.
//

import Foundation
import RxSwift
import RxCocoa

class ArticleViewModel {
   
    private var source: String
    private let managerConnections = ManagerConnections()
    
    init(sourceId: String) {
        self.source = sourceId
    }
    
    func getArticleList() -> Observable<[Article]> {
        return managerConnections.getArticles(source: source)
    }
}
