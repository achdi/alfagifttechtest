//
//  ArticleView.swift
//  alfagift
//
//  Created by Achdi Febriansyah on 11/09/22.
//

import UIKit

class ArticleView: UIView {

    var topView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = #colorLiteral(red: 0.05882352963, green: 0.180392161, blue: 0.2470588237, alpha: 1)
        return view
    }()

    var tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.register(ArticleTableCell.self, forCellReuseIdentifier: ArticleTableCell.identifier)

        tableView.estimatedRowHeight = 200
        tableView.rowHeight = UITableView.automaticDimension
        tableView.allowsSelection = false
        return tableView
    }()
    
    var imageError: UIImageView = {
        let imageError = UIImageView.init(image: UIImage(named: "error_no_result"))
        imageError.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height-180)
        return imageError
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        setTopView()
        setTableView()
        setImageErrorView()
    }

    private func setTopView() {
        addSubview(topView)
        topView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        topView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        topView.widthAnchor.constraint(equalTo: widthAnchor).isActive = true
        topView.heightAnchor.constraint(equalToConstant: 60).isActive = true
    }
    
    private func setImageErrorView() {
        addSubview(imageError)
        imageError.topAnchor.constraint(equalTo: topAnchor, constant: 180).isActive = true
        imageError.contentMode = .scaleAspectFit
        imageError.isHidden = true
    }

    private func setTableView() {
        addSubview(tableView)
        tableView.topAnchor.constraint(equalTo: topView.bottomAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        tableView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    func setTableViewDataSourceDelegate <obj: UITableViewDataSource & UITableViewDelegate> (
        dataSourceDelegate: obj) {
            tableView.dataSource = dataSourceDelegate
            tableView.delegate = dataSourceDelegate
            tableView.reloadData()
    }

}
