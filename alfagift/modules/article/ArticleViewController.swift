//
//  ArticleViewController.swift
//  alfagift
//
//  Created by Achdi Febriansyah on 11/09/22.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa

class ArticleViewController: UIViewController {
    public var viewModel: ArticleViewModel
    public let disposeBag = DisposeBag()
    
    public var articles = [Article]()
    public var filteredArticles = [Article]()
    
    lazy var searchController = SearchController()

    let activityIndicator: UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.medium)
        activityIndicator.color = #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
        activityIndicator.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0);
        
        return activityIndicator
    }()

    override var prefersStatusBarHidden: Bool {
        return true
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        activityIndicator.center = view.center;
        activityIndicator.startAnimating()
        
        view = ArticleView()
        view.addSubview(activityIndicator)
        let tableView = (self.view as! ArticleView).tableView
        tableView.dataSource = self
        tableView.delegate = self
        configureVC()
        displayArticles()
    }
    
    
    init(sourceId: String) {
        viewModel = ArticleViewModel(sourceId: sourceId)
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        NotificationCenter.default.addObserver(self, selector: #selector(ArticleViewController.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ArticleViewController.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func keyboardWillShow(notification: Notification) {
        if let keyboardHeight = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height {
            print("Notification: Keyboard will show")
            (self.view as! ArticleView).tableView.setBottomInset(to: keyboardHeight)
        }
    }

    @objc func keyboardWillHide(notification: Notification) {
        print("Notification: Keyboard will hide")
        (self.view as! ArticleView).tableView.setBottomInset(to: 0.0)
    }
}
