//
//  NetworkRequest.swift
//  alfagift
//
//  Created by Achdi Febriansyah on 10/09/22.
//

import Foundation
enum RequestCompletionType<T> {
  case requestSuccessful(data: T)
  case requestFailure(error: Error)
  case requestEnded
}

enum NetworkRequest {
    static func makeNetworkRequest<T: Decodable>(path: String, query: [URLQueryItem], requestCompleted: @escaping (RequestCompletionType<T>) -> Void) {
    // Observable.create (_ subscribe @escaping AnyObserver<[Movie]> -> Disposable) -> Observer<[Movie]>
    // Creates the observable, returns Observable<[Movie]>
    // Inside, returns the Disposable from the closure parameter
        var queryItems: [URLQueryItem] = query
        queryItems.append(URLQueryItem(name: "apiKey", value: Constants.apiKey))
        queryItems.append(URLQueryItem(name: "language", value: "en"))
        var urlBuilder = URLComponents()
        urlBuilder.scheme = Constants.URL.scheme
        urlBuilder.host = Constants.URL.host
        urlBuilder.path = path
        urlBuilder.queryItems = queryItems
        
        print("URL TO FETCH FROM: \(String(describing: urlBuilder.url))")
        
        guard let url = urlBuilder.url else {
          fatalError("getPopularMovies failed to decode url: \(urlBuilder.string ?? "")")
        }

        print("URL to fetch from: \(url)")

        let session = URLSession.shared
        session.dataTask(with: url) { data, response, error in
          guard let data = data, let response = response as? HTTPURLResponse, error == nil else { return }
          guard response.statusCode == 200 else {
              print("Error, response code is NOT 200")
              let errorStatus = Constants.ErrorResult.custom(string: "Error, response code is NOT 200")
              return requestCompleted(.requestFailure(error: errorStatus))
          }
          do {
//            let data = try JSONDecoder().decode(T.self, from: data)
              let decoder = JSONDecoder()
              let data = try decoder.decode(T.self, from:data)
            /// # Setting `Observer`'s value
            requestCompleted(.requestSuccessful(data: data))
          } catch {
            requestCompleted(.requestFailure(error: error))
            print("Error decoding: \(error)")
          }
          requestCompleted(.requestEnded)
        }.resume()
        session.finishTasksAndInvalidate()
    }
}
