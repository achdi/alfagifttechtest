//
//  ManagerConnection.swift
//  alfagift
//
//  Created by Achdi Febriansyah on 10/09/22.
//

import RxSwift

class ManagerConnections {
  // Create Observable objects
  // https://newsapi.org/v2/everything?category=abc&api_key=ec6114f7a7664cd38209d69c94a2de57
    
    func getSources(category: String) -> Observable<[Source]> {
        let q: [URLQueryItem] = [
            URLQueryItem(name: "category", value: category)
        ]
        return Observable.create { observer in
          NetworkRequest.makeNetworkRequest(path: Constants.EndPoints.urlSources, query: q) { (completion: RequestCompletionType<Sources>) in
            switch completion {
              case .requestSuccessful(data: let data):
                observer.onNext(data.sources)
              case .requestEnded:
                observer.onCompleted()
              case .requestFailure(let error):
                observer.onError(error)
            }
          }
          return Disposables.create()
        }
      }

  // https://newsapi.org/v2/everything?sources=abc&api_key=ec6114f7a7664cd38209d69c94a2de57
  func getArticles(source: String) -> Observable<[Article]> {
      let q: [URLQueryItem] = [
          URLQueryItem(name: "sources", value: source)
      ]
      return Observable.create { observer in
          NetworkRequest.makeNetworkRequest(path: Constants.EndPoints.urlArticle, query: q) { (completion: RequestCompletionType<Articles>) in
            switch completion {
              case .requestSuccessful(data: let data):
                observer.onNext(data.articles)
              case .requestEnded:
                observer.onCompleted()
              case .requestFailure(let error):
                observer.onError(error)
            }
          }
          return Disposables.create()
      }
  }
}
