//
//  Constants.swift
//  alfagift
//
//  Created by Achdi Febriansyah on 10/09/22.
//

import Foundation

// https://newsapi.org/v2/everything?sources=abc&api_key=ec6114f7a7664cd38209d69c94a2de57
enum Constants {
      static let apiKey = "ec6114f7a7664cd38209d69c94a2de57"
      static let apiVersion = "v2/"

    enum URL {
        static let host = "newsapi.org"
        static let scheme = "https"
    }

    enum EndPoints {
        static let urlSources = "/v2/top-headlines/sources"
        static let urlArticle = "/v2/everything"
    }
    
    enum ErrorResult: Error {
        case network(string: String)
        case parser(string: String)
        case custom(string: String)
        
        var localizedDescription: String {
            switch self {
            case .network(let value):   return value
            case .parser(let value):    return value
            case .custom(let value):    return value
            }
        }
    }
}
