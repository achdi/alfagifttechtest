//
//  UserStory.swift
//  alfagift
//
//  Created by Achdi Febriansyah on 10/09/22.
//

import Foundation

struct Article : Decodable{
    let author: String
    let title: String
    let description: String
    let url: String
    let urlToImage: String
    let publishedAt: Date
    let content: String
}
