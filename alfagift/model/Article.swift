//
//  Article.swift
//  alfagift
//
//  Created by Achdi Febriansyah on 10/09/22.
//

import Foundation

struct Articles: Decodable {
    let status: String
    let totalResults: Int
    let articles: [Article]
}

struct Article : Decodable{
    let author: String?
    let title: String?
    let description: String?
    let url: String?
    let urlToImage: String?
    let publishedAt: String?
    let content: String?
}
