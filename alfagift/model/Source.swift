//
//  Source.swift
//  alfagift
//
//  Created by Achdi Febriansyah on 10/09/22.
//

import Foundation

struct Sources: Decodable {
    let status: String
    let sources: [Source]
}

struct Source : Decodable{
    let id: String
    let name: String
}
